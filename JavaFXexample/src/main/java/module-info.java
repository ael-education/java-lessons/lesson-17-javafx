module ru.pnu.javafxexample {
    requires javafx.controls;
    requires javafx.fxml;

    opens ru.pnu.javafxexample to javafx.fxml;
    exports ru.pnu.javafxexample;
}
